    For synthetic data set: 
    python main.py synthetic --num_workers 5 --num_rounds 1000 --bz 256 --num_local_steps 10 --device "cuda" --gnetwork_num_epochs 30000 --num_trials_to_decode 20 --lr 0.001 --adv_lr 0.001 --sigma 0.1 --gnetwork_features 1000 --start_point global_model --decoded_epochs 20000 --DP --epsilon 1
    
    For Adult data set: 
    python main.py adult --num_workers 10 --num_rounds 1000 --bz 1024 --device "cuda" --gnetwork_num_epochs 10000 --num_trials_to_decode 1 --lr 0.001 --adv_lr 0.001 --gnetwork_features 1000 --start_point global_model --decoded_epochs 2000 --use_weighted_average --runs 10

    For cifar10 data set: 
    python main.py cifar10 --num_workers 10 --num_rounds 1 --bz 128 --num_local_steps 1 --device "cuda" --gnetwork_num_epochs 10000 --num_trials_to_decode 2 --lr 0.01 --adv_lr 0.0001 --sigma 0.1 --gnetwork_features 1000 --start_point global_model --decoded_epochs 1000 --runs 1 --model conv --fit_by_epoch --gnetwork_type nn_multiple_linear
