import json
import numpy as np
import scipy
import matplotlib.pyplot as plt
import seaborn as sns


def TwoSampleT2Test(X, Y):
    nx, p = X.shape
    ny, _ = Y.shape
    delta = np.mean(X, axis=0) - np.mean(Y, axis=0)
    Sx = np.cov(X, rowvar=False)
    Sy = np.cov(Y, rowvar=False)
    S_pooled = ((nx-1)*Sx + (ny-1)*Sy)/(nx+ny-2)
    t_squared = (nx*ny)/(nx+ny) * np.matmul(np.matmul(delta.transpose(), np.linalg.inv(S_pooled)), delta)
    statistic = t_squared * (nx+ny-p-1)/(p*(nx+ny-2))

    f_function = scipy.stats.f(p, nx+ny-p-1)
    p_value = 1 - f_function.cdf(statistic)
    return statistic, p_value

if __name__ == '__main__':
    stat_t2 = np.zeros([10,10])
    stat_p = np.ones([10,10])
    data_set = []
    data_pv = []
    js_distance = np.zeros([10,10])
    for i in range(10):
        with open('train/' + str(i) + ".json", 'rb') as f:
            data = json.load(f)
            num_classes = data['num_classes']
            prob_vector = np.zeros(num_classes)
            data_array = np.array(data['x'])
            for i in range(num_classes):
                prob_vector[i] = sum([1 for j in data['y'] if j==i])/len(data['y'])
            data_pv.append(np.array(prob_vector))
            data_array_with_class = np.c_[data_array, np.array(data['y'])]
            print(i, data_array_with_class.shape)
            data_set.append(data_array_with_class)

    for i in range(10):
        for j in range(10):
            s, p = TwoSampleT2Test(data_set[i], data_set[j])
            stat_t2[i,j] = s
            stat_p[i,j] = p
            js = scipy.spatial.distance.jensenshannon(data_pv[i], data_pv[j])
            js_distance[i,j] = js
    plt.rcParams.update({'font.size': 12})
    #ax = sns.heatmap(stat_t2, linewidth=0.5, cmap="YlGnBu")
    ax = sns.heatmap(js_distance, linewidth=0.5, cmap="YlGnBu")
    ax.invert_yaxis()
    #plt.title('label jensen shannon distance')
    plt.title('Hotelling’s T^2 test')
    plt.show()